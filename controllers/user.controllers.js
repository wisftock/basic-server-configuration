const { response } = require('express');

const userGET = (req, res = response, next) => {
  res.json({
    msg: 'GET API Controller',
  });
};
const userPOST = (req, res = response, next) => {
  res.json({
    msg: 'POST API Controller',
  });
};

const userPUT = (req, res = response, next) => {
  res.json({
    msg: 'PUT API Controller',
  });
};

const userPATCH = (req, res = response, next) => {
  res.json({
    msg: 'PATCH API Controller',
  });
};

const userDELETE = (req, res = response, next) => {
  res.json({
    msg: 'DELETE API Controller',
  });
};

module.exports = {
  userGET,
  userPOST,
  userPUT,
  userPATCH,
  userDELETE,
};
